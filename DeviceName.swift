//
//  DeviceName.swift
//
//
//  Created by Julien Widmer on 17.02.20.
//

import UIKit

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)

        let machineMirror = Mirror(reflecting: systemInfo.machine)

        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        return identifier
    }
}



struct Device {
    var model: String
    var name: String

    init(model: String, name: String) {
        self.model = model
        self.name = name
    }
}



func getDeviceName() -> String {
    let deviceModel = UIDevice.current.modelName

    let devices: [Device] = [
        // Simulator
        Device(model: "i386", name: "32-bit Simulator"),
        Device(model: "x86_64", name: "64-bit Simulator"),
        // iPhone
        // https://support.apple.com/en-us/HT201296
        Device(model: "iPhone1,1", name: "iPhone"),
        Device(model: "iPhone1,2", name: "iPhone 3G"),
        Device(model: "iPhone2,1", name: "iPhone 3GS"),
        Device(model: "iPhone3,1", name: "iPhone 4"),
        Device(model: "iPhone3,2", name: "iPhone 4"),
        Device(model: "iPhone3,3", name: "iPhone 4"),
        Device(model: "iPhone4,1", name: "iPhone 4S"),
        Device(model: "iPhone4,2", name: "iPhone 4S"),
        Device(model: "iPhone4,3", name: "iPhone 4S"),
        Device(model: "iPhone5,1", name: "iPhone 5"),
        Device(model: "iPhone5,2", name: "iPhone 5"),
        Device(model: "iPhone5,3", name: "iPhone 5C (GSM/CDMA)"),
        Device(model: "iPhone5,4", name: "iPhone 5C (GSM)"),
        Device(model: "iPhone6,1", name: "iPhone 5S (GSM)"),
        Device(model: "iPhone6,2", name: "iPhone 5S (GSM/CDMA)"),
        Device(model: "iPhone7,2", name: "iPhone 6"),
        Device(model: "iPhone7,1", name: "iPhone 6 Plus"),
        Device(model: "iPhone8,1", name: "iPhone 6S"),
        Device(model: "iPhone8,2", name: "iPhone 6S Plus"),
        Device(model: "iPhone8,4", name: "iPhone SE"),
        Device(model: "iPhone9,1", name: "iPhone 7"),
        Device(model: "iPhone9,3", name: "iPhone 7"),
        Device(model: "iPhone9,2", name: "iPhone 7 Plus"),
        Device(model: "iPhone9,4", name: "iPhone 7 Plus"),
        Device(model: "iPhone10,1", name: "iPhone 8"),
        Device(model: "iPhone10,4", name: "iPhone 8"),
        Device(model: "iPhone10,2", name: "iPhone 8 Plus"),
        Device(model: "iPhone10,5", name: "iPhone 8 Plus"),
        Device(model: "iPhone10,3", name: "iPhone X"),
        Device(model: "iPhone10,6", name: "iPhone X"),
        Device(model: "iPhone11,2", name: "iPhone XS"),
        Device(model: "iPhone11,6", name: "iPhone XS Max"),
        Device(model: "iPhone11,8", name: "iPhone XR"),
        Device(model: "iPhone12,1", name: "iPhone 11"),
        Device(model: "iPhone12,3", name: "iPhone 11 Pro"),
        Device(model: "iPhone12,5", name: "iPhone 11 Pro Max"),
        Device(model: "iPhone12,8", name: "iPhone SE (2nd generation)"),
        // iPad
        // https://support.apple.com/en-us/HT201471
        Device(model: "iPad1,1", name: "iPad"),
        Device(model: "iPad2,1", name: "iPad 2"),
        Device(model: "iPad2,2", name: "iPad 2"),
        Device(model: "iPad2,3", name: "iPad 2"),
        Device(model: "iPad2,4", name: "iPad 2"),
        Device(model: "iPad2,5", name: "iPad mini"),
        Device(model: "iPad2,6", name: "iPad mini"),
        Device(model: "iPad2,7", name: "iPad mini"),
        Device(model: "iPad3,1", name: "iPad (3rd generation)"),
        Device(model: "iPad3,2", name: "iPad (3rd generation)"),
        Device(model: "iPad3,3", name: "iPad (3rd generation)"),
        Device(model: "iPad3,4", name: "iPad (4th generation)"),
        Device(model: "iPad3,5", name: "iPad (4th generation)"),
        Device(model: "iPad3,6", name: "iPad (4th generation)"),
        Device(model: "iPad4,1", name: "iPad Air"),
        Device(model: "iPad4,2", name: "iPad Air"),
        Device(model: "iPad4,3", name: "iPad Air"),
        Device(model: "iPad4,4", name: "iPad mini 2"),
        Device(model: "iPad4,5", name: "iPad mini 2"),
        Device(model: "iPad4,6", name: "iPad mini 2"),
        Device(model: "iPad4,7", name: "iPad mini 3"),
        Device(model: "iPad4,8", name: "iPad mini 3"),
        Device(model: "iPad4,9", name: "iPad mini 3"),
        Device(model: "iPad5,1", name: "iPad mini 4"),
        Device(model: "iPad5,2", name: "iPad mini 4"),
        Device(model: "iPad5,3", name: "iPad Air 2"),
        Device(model: "iPad5,4", name: "iPad Air 2"),
        Device(model: "iPad6,3", name: "iPad Pro (9.7-inch)"),
        Device(model: "iPad6,4", name: "iPad Pro (9.7-inch)"),
        Device(model: "iPad6,7", name: "iPad Pro (12.9-inch)"),
        Device(model: "iPad6,8", name: "iPad Pro (12.9-inch)"),
        Device(model: "iPad6,11", name: "iPad (5th generation)"),
        Device(model: "iPad6,12", name: "iPad (5th generation)"),
        Device(model: "iPad7,1", name: "iPad Pro 12.9-inch (2nd generation)"),
        Device(model: "iPad7,2", name: "iPad Pro 12.9-inch (2nd generation)"),
        Device(model: "iPad7,3", name: "iPad Pro (9.7-inch)"),
        Device(model: "iPad7,4", name: "iPad Pro (10.5-inch)"),
        Device(model: "iPad7,5", name: "iPad (6th generation)"),
        Device(model: "iPad7,6", name: "iPad (6th generation) (Wi-Fi + Cellular)"),
        Device(model: "iPad7,11", name: "iPad (7th generation) (Wi-Fi)"),
        Device(model: "iPad7,12", name: "iPad (7th generation) (Wi-Fi + Cellular)"),
        Device(model: "iPad8,1", name: "iPad Pro 11-inch"),
        Device(model: "iPad8,2", name: "iPad Pro 11-inch"),
        Device(model: "iPad8,3", name: "iPad Pro 11-inch"),
        Device(model: "iPad8,4", name: "iPad Pro 11-inch"),
        Device(model: "iPad8,5", name: "iPad Pro 12.9-inch (3rd generation)"),
        Device(model: "iPad8,6", name: "iPad Pro 12.9-inch (3rd generation)"),
        Device(model: "iPad8,7", name: "iPad Pro 12.9-inch (3rd generation)"),
        Device(model: "iPad8,8", name: "iPad Pro 12.9-inch (3rd generation)"),
        Device(model: "iPad8,9", name: "iPad Pro 11-inch (2nd generation) (Wi-Fi)"),
        Device(model: "iPad8,10", name: "iPad Pro 11-inch (2nd generation) (Wi-Fi + Cellular)"),
        Device(model: "iPad8,11", name: "iPad Pro 12.9-inch (4th generation) (Wi-Fi)"),
        Device(model: "iPad8,12", name: "iPad Pro 12.9-inch (4th generation) (Wi-Fi + Cellular)"),
        Device(model: "iPad11,1", name: "iPad mini (5th generation)"),
        Device(model: "iPad11,2", name: "iPad mini (5th generation)"),
        Device(model: "iPad11,3", name: "iPad Air (3rd generation)"),
        Device(model: "iPad11,4", name: "iPad Air (3rd generation)"),
        // Apple Watch
        // https://support.apple.com/en-us/HT204507
        Device(model: "Watch1,1", name: "Apple Watch (1st generation) - 38mm"),
        Device(model: "Watch1,2", name: "Apple Watch (1st generation) - 38mm"),
        Device(model: "Watch2,6", name: "Apple Watch Series 1 - 38mm"),
        Device(model: "Watch2,7", name: "Apple Watch Series 1 - 42mm"),
        Device(model: "Watch2,3", name: "Apple Watch Series 2 - 38mm"),
        Device(model: "Watch2,4", name: "Apple Watch Series 2 - 42mm"),
        Device(model: "Watch3,1", name: "Apple Watch Series 3 (GPS + Cellular) - 38mm"),
        Device(model: "Watch3,2", name: "Apple Watch Series 3 (GPS + Cellular) - 42mm"),
        Device(model: "Watch3,3", name: "Apple Watch Series 3 (GPS) - 38mm"),
        Device(model: "Watch3,4", name: "Apple Watch Series 3 (GPS) - 42mm"),
        Device(model: "Watch4,1", name: "Apple Watch Series 4 (GPS) - 40mm"),
        Device(model: "Watch4,2", name: "Apple Watch Series 4 (GPS) - 44mm"),
        Device(model: "Watch4,3", name: "Apple Watch Series 4 (GPS + Cellular) - 40mm"),
        Device(model: "Watch4,4", name: "Apple Watch Series 4 (GPS + Cellular) - 44mm"),
        Device(model: "Watch5,1", name: "Apple Watch Series 5 (GPS) - 40mm"),
        Device(model: "Watch5,2", name: "Apple Watch Series 5 (GPS) - 44mm"),
        Device(model: "Watch5,3", name: "Apple Watch Series 5 (GPS + Cellular) - 40mm"),
        Device(model: "Watch5,4", name: "Apple Watch Series 5 (GPS + Cellular) - 44mm"),
        //iPod Touch
        // https://support.apple.com/en-us/HT204217
        Device(model: "iPod1,1", name: "iPod touch"),
        Device(model: "iPod2,1", name: "iPod touch (2nd generation)"),
        Device(model: "iPod3,1", name: "iPod touch (3rd generation)"),
        Device(model: "iPod4,1", name: "iPod touch (4th generation)"),
        Device(model: "iPod5,1", name: "iPod touch (5th generation)"),
        Device(model: "iPod7,1", name: "iPod touch (6th generation)"),
        Device(model: "iPod9,1", name: "iPod touch (7th generation)")
    ]

    var name = deviceModel

    for device in devices {
        if deviceModel == device.model {
            name = device.name
            break
        }
    }

    return name
}
