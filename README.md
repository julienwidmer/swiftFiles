# Swift Files
Useful extensions, classes and functions

#  Table of contents
- [Features](#features)
- [Documentation](#documentation)
    - [DeviceName.swift](#devicename)
    - [Gradient.swift](#gradient)
    - [SketchShadow.swift](#sketchshadow)
- [Applications who use this project](#applications-who-use-this-project)
- [Author](#author)

#  Features
* [DeviceName.swift](DeviceName.swift) - Get the iPhone, iPad, Apple Watch or iPod touch name.
* [Gradient.swift](Gradient.swift) - Create a gradient and insert it as a sublayer of UIView.
* [SketchShadow.swift](SketchShadow.swift) - Create a shadow with the same settings that you would have used in [Sketch](https://www.sketch.com).

# Documentation

## DeviceName
Get the iPhone, iPad, Apple Watch or iPod touch name.
### How it works
The `DeviceName.swift` file can be divided in 3 parts, there is the extension of UIDevice to get the model identifier, a struct named `Device` who is gonna be used to create an array which hold each device's identifier and name and the function to get the device name.
### How to use
1) Import [DeviceName.swift](DeviceName.swift) in your Xcode project or copy/paste the code.
2) Use the function `getDeviceName()` to get the device name as a `String`.
```swift
let name = getDeviceName()
```

## Gradient
Create a gradient and insert it as a sublayer of UIView.
### How it works
The `Gradient.swift` is an extension of UIView with one function who creates a `CAGradientLayer()` from an array of `UIColor` and adds it as sublayer.
### How to use
1) Import [Gradient.swift](Gradient.swift) in your Xcode project or copy/paste the code.
2) Use the function `applyGradient(colours: [UIColor])` on a `UIView`.
```swift
view.applyGradient(colours: [UIColor.red, UIColor.orange])
```

## SketchShadow
Create a shadow with the same settings that you would have used in [Sketch](https://www.sketch.com).
### How it works
The `SketchShadow.swift` is an extension of CALayer with one function who adds a shadow to the layer.
### How to use
1) Import [SketchShadow.swift](SketchShadow.swift) in your Xcode project or copy/paste the code.
2) Use the function `applySketchShadow(color: UIColor, alpha: Float, x: CGFloat, y: CGFloat, blur: CGFloat, spread: CGFloat)` on the layer.
```swift
view.layer.applySketchShadow(color: .black, alpha: 0.5, x: 1, y: 3, blur: 5, spread: 0)
```

# Applications who use this project
* [Fufio](https://apps.apple.com/us/app/fufio/id1306084813) - The app uses [DeviceName.swift](DeviceName.swift) to gather device information for feedback submissions.


# Author
Hi! I’m [Julien Widmer](https://www.julienwidmer.ca) - Feel free to send me an [Email](mailto:hello@julienwidmer.ca) or to follow me on [Twitter](http://twitter.com/Qasph).