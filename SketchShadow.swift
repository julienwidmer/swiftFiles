//
//  SketchShadow.swift
//  
//
//  Created by Julien Widmer on 20.02.20.
//

import UIKit

extension CALayer {
    // Sketch Shadow Settings
    // https://stackoverflow.com/questions/34269399/how-to-control-shadow-spread-and-blur
    func applySketchShadow(color: UIColor, alpha: Float, x: CGFloat, y: CGFloat, blur: CGFloat, spread: CGFloat) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
    
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
